const array = ["B$u$i$ld", "$t$$h$e", "N$e$x$t", "E$$ra", "$$o$f$", "S$$of$t$wa$r$e", "De$$ve$l$op$me$n$t"];
let result = '';
let cleanValue = '';

array.map(
    value => {
        cleanValue = value;
        while(cleanValue.includes('$')) {
            cleanValue = cleanValue.replace('$', '');
        }
        result = result + cleanValue + ' ';
    }
);

result = result.slice(0, result.length - 1);
result = result.toUpperCase();

console.log(result);